package com.tarbye.crossroad;

import java.io.OutputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;

/**
 * Config class pre upraveny format system out-u
 */
public class CustomPrintStream extends PrintStream {

    public CustomPrintStream(OutputStream out) {
        super(out);
    }

    @Override
    public void println(String string) {
        super.println("[" + LocalDateTime.now() + "] " + string);
    }
}