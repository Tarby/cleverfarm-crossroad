package com.tarbye.crossroad;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import com.tarbye.crossroad.trafficlight.TrafficLight;
import com.tarbye.crossroad.trafficlight.TwoColorTrafficLight;
import com.tarbye.crossroad.vehicle.AbstractVehicle;
import com.tarbye.crossroad.vehicle.Car;

public class CrossroadApp {

    //pocet aut za minutu z NORTH
    private static final int C1 = 5;
    //pocet aut za minutu z SOUTH
    private static final int C2 = 5;
    //pocet aut za minutu z WEST
    private static final int C3 = 5;
    //pocet aut za minutu z EAST
    private static final int C4 = 5;

    //interval zmeny pre semafor NORTH <--> SOUTH
    private static final int T1 = 60;
    //interval zmeny pre semafor WEST <--> EAST
    private static final int T2 = 60;

    //interval, za ktory auto opusti krizovatku, ak ma zelenu a je prvy v poradi
    private static final int S1 = 10;

    public static void main(String[] args) {
        System.setOut(new CustomPrintStream(System.out));

        final Set<AbstractVehicle> cars = Set.of(
                new Car(MovingDirection.NORTH_TO_SOUTH, C1, S1),
                new Car(MovingDirection.SOUTH_TO_NORTH, C2, S1),
                new Car(MovingDirection.WEST_TO_EAST, C3, S1),
                new Car(MovingDirection.EAST_TO_WEST, C4, S1)
        );

        FourWayCrossroad crossroad = new FourWayCrossroad(builTrafficLights(MovingDirection.values()), cars);
        //spustenie simulacie
        crossroad.simulate();
    }

    /**
     * Pomocna metoda na vytovorenie objektov semaforov
     *
     * @param movingDirections Povolene smery danej krizovatky
     * @return Set semaforov pre krizovatku
     */
    private static Set<TrafficLight> builTrafficLights(MovingDirection[] movingDirections) {
        return Arrays.stream(movingDirections)
                .map(cd -> {
                    if (MovingDirection.NORTH_TO_SOUTH.equals(cd)) {
                        return new TwoColorTrafficLight(MovingDirection.NORTH_TO_SOUTH, T1, true);
                    } else if (MovingDirection.SOUTH_TO_NORTH.equals(cd)) {
                        return new TwoColorTrafficLight(MovingDirection.SOUTH_TO_NORTH, T1, true);
                    } else if (MovingDirection.EAST_TO_WEST.equals(cd)) {
                        return new TwoColorTrafficLight(MovingDirection.EAST_TO_WEST, T2, false);
                    } else
                        return new TwoColorTrafficLight(MovingDirection.WEST_TO_EAST, T2, false);
                })
                .collect(Collectors.toSet());
    }
}
