package com.tarbye.crossroad;

/**
 * Enum reprezentujuci zakladne smery jazdy.
 */
public enum MovingDirection {
    NORTH_TO_SOUTH, SOUTH_TO_NORTH, EAST_TO_WEST, WEST_TO_EAST;

    //priesecniky ciest(smerov)
    public MovingDirection getIntersectDirection() {
        return switch (this) {
            case NORTH_TO_SOUTH -> EAST_TO_WEST;
            case SOUTH_TO_NORTH -> WEST_TO_EAST;
            case EAST_TO_WEST -> NORTH_TO_SOUTH;
            case WEST_TO_EAST -> SOUTH_TO_NORTH;
            default -> throw new IllegalArgumentException();
        };
    }
}
