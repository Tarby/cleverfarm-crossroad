package com.tarbye.crossroad;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.stream.Collectors;
import com.tarbye.crossroad.event.Event;
import com.tarbye.crossroad.event.EventTimeComparator;
import com.tarbye.crossroad.event.TrafficLightSwitchEvent;
import com.tarbye.crossroad.event.VehicleCrossEvent;
import com.tarbye.crossroad.event.VehicleGenerateEvent;
import com.tarbye.crossroad.trafficlight.TrafficLight;
import com.tarbye.crossroad.vehicle.AbstractVehicle;

/**
 * Implementacia jednoduchej krizovatky obsluhujuca rozne typy eventov.
 */
public class FourWayCrossroad extends AbstractCrossroad {

    private final PriorityQueue<Event> eventQueue;
    //TODO: zmenit Integer na realny objekt auta? Ma to v tomto pripade nejaku vyhodu?
    //pocitadlo aut cakajucich v rade
    private final Map<MovingDirection, Integer> waitingCarsCounter;

    public FourWayCrossroad(Set<TrafficLight> trafficLights, Set<AbstractVehicle> vehicles) {
        super(trafficLights, vehicles);

        this.eventQueue = new PriorityQueue<>(new EventTimeComparator());
        this.waitingCarsCounter = new HashMap<>();

        //naschedulovanie prveho zapnutia/prepnutia semaforov
        eventQueue.add(new TrafficLightSwitchEvent(
                LocalDateTime.now(),
                getTrafficLightByByMovingDirection(MovingDirection.EAST_TO_WEST),
                MovingDirection.EAST_TO_WEST)
        );
        eventQueue.add(new TrafficLightSwitchEvent(
                LocalDateTime.now(),
                getTrafficLightByByMovingDirection(MovingDirection.WEST_TO_EAST),
                MovingDirection.WEST_TO_EAST)
        );

        generateVehicles();
    }

    /**
     * Slucka obsluhujuca eventy.
     * Slucka je ukoncena, ak sa v queue nenachadza ziadny novy event.
     */
    @Override public void simulate() {
        while (!eventQueue.isEmpty()) {
            // vybratie prveho eventu z queue
            final Event head = eventQueue.peek();

            //TODO: presne riesenie event time-u?
            //ak je nieco v queue a je cas to spracovat, opripade sa to nestihlo spracovatp
            //poradie eventov je zachovane, aj v pripade neskorieho spracovania
            if (head != null && (head.getEventTime().isBefore(LocalDateTime.now()) || head.getEventTime().isEqual(LocalDateTime.now()))) {
                if (head instanceof VehicleGenerateEvent event) {
                    System.out.println("Incomming vehicle to crossroad from direction: " + event.getVehicle().getMovingDirection().name());
                    //increment pocitadla cakajucich aut pre dany smer pohybu
                    waitingCarsCounter.merge(event.getVehicle().getMovingDirection(), 1, (o, n) -> o + 1);
                    //schedulovanie prichodu noveho vozidla pre dany smer
                    eventQueue.add(new VehicleGenerateEvent(
                            LocalDateTime.now().plusSeconds(event.getVehicle().getIntervalBetweenGeneratedVehicles()),
                            event.getVehicle())
                    );
                    //odstranenie eventu z queue
                    eventQueue.remove();
                    printWaitingCarCounter();
                } else if (head instanceof VehicleCrossEvent event) {
                    System.out.println("Vehicle may cross crossroad for direction: " + event.getCrossingDirection().name());

                    //znizenie pocitadla, aby sa zohladnilo odchadzajuce auto
                    waitingCarsCounter.merge(event.getCrossingDirection(), 0, (o, n) -> o == 0 ? 0 : (o - 1));

                    printWaitingCarCounter();

                    // TODO: co ak je viacero typov vozidla pre dany smer?
                    final AbstractVehicle movingVehicle = getVehicles().stream()
                            .filter(vehicle -> vehicle.getMovingDirection().equals(event.getCrossingDirection()))
                            .findFirst()
                            .orElseThrow();
                    //naschedulovanie dalsieho vozidla, ktore moze prejst krizovatkou
                    eventQueue.add(new VehicleCrossEvent(
                            LocalDateTime.now().plusSeconds(movingVehicle.getVehicleCrossingTime()),
                            event.getCrossingDirection())
                    );
                    //odstranenie eventu z queue
                    eventQueue.remove();
                } else if (head instanceof TrafficLightSwitchEvent event) {
                    //fyzicka zmena svetiel na semafore
                    event.getTrafficLight().switchLight();

                    final MovingDirection oppositeTrafficLightDirection = event.getAllowedDirection().getIntersectDirection();

                    //naschedulovanie konca intervalu, dokedy sa da jazdit tymtonovym smerom
                    eventQueue.add(new TrafficLightSwitchEvent(
                            LocalDateTime.now().plusSeconds(getTrafficLightByByMovingDirection(oppositeTrafficLightDirection).getLightChangeInterval()),
                            getTrafficLightByByMovingDirection(oppositeTrafficLightDirection),
                            oppositeTrafficLightDirection)
                    );

                    final Set<Event> unnecessaryCrossEvents = eventQueue.stream()
                            .filter(e -> e instanceof VehicleCrossEvent ev && ev.getCrossingDirection().equals(event.getAllowedDirection()))
                            .collect(Collectors.toSet());
                    //odstranenie zvysnych naschedulovanych crossing eventov pre tento smer
                    eventQueue.removeAll(unnecessaryCrossEvents);

                    //schedulovanie crossing eventu pre zmeneny smer semaforov
                    //semafor sa zmenil, auta mozu zacat prudit
                    eventQueue.add(new VehicleCrossEvent(LocalDateTime.now(), oppositeTrafficLightDirection));
                    //odstranenie eventu z queue
                    eventQueue.remove();
                    System.out.println("Traffic light switched to direction: " + oppositeTrafficLightDirection.name() + " Crossing of cars scheduled.");
                }
            }
        }
    }

    /**
     * Pociatocne generovanie prichodu vozidla pre kazdy mozny smer
     */
    private void generateVehicles() {
        for (AbstractVehicle vehicleType : getVehicles()) {
            if (MovingDirection.NORTH_TO_SOUTH.equals(vehicleType.getMovingDirection())) {
                eventQueue.add(new VehicleGenerateEvent(
                        LocalDateTime.now().plusSeconds(vehicleType.getIntervalBetweenGeneratedVehicles()),
                        vehicleType)
                );
            } else if (MovingDirection.SOUTH_TO_NORTH.equals(vehicleType.getMovingDirection())) {
                eventQueue.add(new VehicleGenerateEvent(
                        LocalDateTime.now().plusSeconds(vehicleType.getIntervalBetweenGeneratedVehicles()),
                        vehicleType)
                );
            } else if (MovingDirection.EAST_TO_WEST.equals(vehicleType.getMovingDirection())) {
                eventQueue.add(new VehicleGenerateEvent(
                        LocalDateTime.now().plusSeconds(vehicleType.getIntervalBetweenGeneratedVehicles()),
                        vehicleType)
                );
            } else if (MovingDirection.WEST_TO_EAST.equals(vehicleType.getMovingDirection())) {
                eventQueue.add(new VehicleGenerateEvent(
                        LocalDateTime.now().plusSeconds(vehicleType.getIntervalBetweenGeneratedVehicles()),
                        vehicleType)
                );
            }
        }
    }

    private void printWaitingCarCounter() {
        System.out.println("Printing new state of waiting lines");
        waitingCarsCounter.forEach((key, value) -> System.out.println("Count of cars waiting in line " + key + " is " + value));
    }
}
