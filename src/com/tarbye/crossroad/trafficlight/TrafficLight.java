package com.tarbye.crossroad.trafficlight;

import com.tarbye.crossroad.MovingDirection;

public interface TrafficLight {

    /**
     * Prepinac farieb na semafore
     */
    void switchLight();

    /**
     * Check, ci svieti na semafore cervena.
     *
     * @return true ak svieti cervena, false ak svieti ina farba
     */
    boolean isRedLightOn();

    /**
     * @return povoleny smer jazdy cez semafor
     */
    MovingDirection getDirectionToAllowCar();

    /**
     * @return interval v sekundach, kedy sa meni cervena na zelenu a opacne
     */
    int getLightChangeInterval();
}
