package com.tarbye.crossroad.trafficlight;

import com.tarbye.crossroad.MovingDirection;

/**
 * Trieda reprezentujuca semafor. Oproti klasickemu semaforu podporuje len cervenu a zelenu farbu.
 */
public class TwoColorTrafficLight implements TrafficLight {

    //povoleny smer jazdy cez semafor
    private final MovingDirection directionToAllowCar;
    //interval zmeny zelenej na cervenu a opacne
    private final int lightChangeInterval;
    //holder pre aktualne svietaciu farbu
    private boolean redLightOn;

    public TwoColorTrafficLight(MovingDirection directionToAllowCar, int lightChangeInterval, boolean redLightOn) {
        this.directionToAllowCar = directionToAllowCar;
        this.lightChangeInterval = lightChangeInterval;
        this.redLightOn = redLightOn;
    }

    @Override
    public MovingDirection getDirectionToAllowCar() {
        return directionToAllowCar;
    }

    @Override
    public int getLightChangeInterval() {
        return lightChangeInterval;
    }

    @Override
    public boolean isRedLightOn() {
        return redLightOn;
    }

    @Override
    public void switchLight() {
        this.redLightOn = !isRedLightOn();
        System.out.println("Traffic light switched to " + (redLightOn ? "red" : "green") + " for direction " + directionToAllowCar.name());
    }
}
