package com.tarbye.crossroad.event;

import java.time.LocalDateTime;
import com.tarbye.crossroad.MovingDirection;
import com.tarbye.crossroad.trafficlight.TrafficLight;

/**
 * Event reprezentujuci prepnutie svetiel na semafore
 */
public class TrafficLightSwitchEvent extends Event {
    //povoleny smer chodu vozidiel
    private final MovingDirection allowedDirection;
    //semafory obsluhujuce krizovatku, pre ktoru prebieha event
    private final TrafficLight trafficLight;

    public TrafficLightSwitchEvent(LocalDateTime eventTime, TrafficLight trafficLight, MovingDirection allowedDirection) {
        this.eventTime = eventTime;
        this.trafficLight = trafficLight;
        this.allowedDirection = allowedDirection;
    }

    public MovingDirection getAllowedDirection() {
        return allowedDirection;
    }

    public TrafficLight getTrafficLight() {
        return trafficLight;
    }
}
