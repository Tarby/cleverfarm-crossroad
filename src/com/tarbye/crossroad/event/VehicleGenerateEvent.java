package com.tarbye.crossroad.event;

import java.time.LocalDateTime;
import com.tarbye.crossroad.vehicle.AbstractVehicle;

/**
 * Event reprezentujuci prichod vozidla ku krizovatke
 */
public class VehicleGenerateEvent extends Event {

    //prichadzajuce vozidlo
    private AbstractVehicle vehicle;

    public VehicleGenerateEvent(LocalDateTime eventTime, AbstractVehicle vehicle) {
        this.eventTime = eventTime;
        this.vehicle = vehicle;
    }

    public AbstractVehicle getVehicle() {
        return vehicle;
    }
}
