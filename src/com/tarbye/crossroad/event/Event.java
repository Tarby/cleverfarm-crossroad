package com.tarbye.crossroad.event;

import java.time.LocalDateTime;

/**
 * Basic event
 */
public class Event {

    //cas, kedy sa ma event vykonat
    protected LocalDateTime eventTime;

    public LocalDateTime getEventTime() {
        return eventTime;
    }
}
