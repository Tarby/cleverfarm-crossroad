package com.tarbye.crossroad.event;

import java.time.LocalDateTime;
import com.tarbye.crossroad.MovingDirection;

/**
 * Event reprezentujuci udalost, kedy auto prejde krizovatkou
 */
public class VehicleCrossEvent extends Event {

    //smer pohybujuceho sa vozidla
    private final MovingDirection crossingDirection;

    public VehicleCrossEvent(LocalDateTime eventTime, MovingDirection crossingDirection) {
        this.eventTime = eventTime;
        this.crossingDirection = crossingDirection;
    }

    public MovingDirection getCrossingDirection() {
        return crossingDirection;
    }
}
