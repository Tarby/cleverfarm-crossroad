package com.tarbye.crossroad.event;

import java.util.Comparator;

/**
 * Custom comparator pre event queue.
 */
public class EventTimeComparator implements Comparator<Event> {

    /**
     * Porovnava schedulovany cas dvojice eventov.
     *
     * @return  -1 ak event1 nastane pred eventom2,
     *          +1 ak event2 nastane pred eventom1,
     *          0 ak su casy vykonania oboch eventov rovnake.
     */
    @Override
    public int compare(Event e1, Event e2) {
        if (e1.getEventTime().isBefore(e2.getEventTime())) {
            return -1;
        } else if (e1.getEventTime().isAfter(e2.getEventTime())) {
            return 1;
        } else return 0;
    }
}




