package com.tarbye.crossroad;

import java.util.Set;
import com.tarbye.crossroad.trafficlight.TrafficLight;
import com.tarbye.crossroad.vehicle.AbstractVehicle;

/**
 * Predstavuje vseobecnu krizovatku obsahujucu semafory a obsluhujucu povolene vozidla.
 */
public abstract class AbstractCrossroad {
    //semafory nachadzajuce sa na krizovatke
    private final Set<TrafficLight> trafficLights;
    //povolene/mozne typy vozidiel
    private final Set<AbstractVehicle> vehicles;

    public AbstractCrossroad(Set<TrafficLight> trafficLights, Set<AbstractVehicle> vehicles) {
        this.trafficLights = trafficLights;
        this.vehicles = vehicles;
    }

    //vlastna simulacia krizovatky
    public abstract void simulate();

    public Set<TrafficLight> getTrafficLights() {
        return trafficLights;
    }

    public TrafficLight getTrafficLightByByMovingDirection(MovingDirection dir) {
        return getTrafficLights().stream()
                .filter(tl -> tl.getDirectionToAllowCar().equals(dir))
                .findFirst()
                .orElseThrow();
    }

    public Set<AbstractVehicle> getVehicles() {
        return vehicles;
    }
}
