package com.tarbye.crossroad.vehicle;

import com.tarbye.crossroad.MovingDirection;

/**
 * Predstavuje konkretnu implementaciu vozidla.
 */
public class Car extends AbstractVehicle {

    public Car(MovingDirection movingDirection, int generatedPerMinute, int vehicleCrossingTime) {
        super(movingDirection, generatedPerMinute, vehicleCrossingTime);
    }
}
