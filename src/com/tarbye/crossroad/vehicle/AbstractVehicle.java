package com.tarbye.crossroad.vehicle;

import com.tarbye.crossroad.MovingDirection;

/**
 * Trieda predstavuje vseobecne vozidlo
 */
public abstract class AbstractVehicle {
    //smer, ktorym sa vozidlo hybe
    private final MovingDirection movingDirection;
    //pocet vozidiel vygenerovanych za minutu
    private final int vehiclesGeneratedPerMinute;
    private final int vehicleCrossingTime;

    public AbstractVehicle(MovingDirection movingDirection, int vehiclesGeneratedPerMinute, int vehicleCrossingTime) {
        this.movingDirection = movingDirection;
        this.vehiclesGeneratedPerMinute = vehiclesGeneratedPerMinute;
        this.vehicleCrossingTime = vehicleCrossingTime;
    }

    public MovingDirection getMovingDirection() {
        return movingDirection;
    }

    // TODO: nedomyslene pre necele cisla
    // auto kazdych x sekund
    public int getIntervalBetweenGeneratedVehicles() {
        return 60 / this.vehiclesGeneratedPerMinute;
    }

    public int getVehicleCrossingTime() {
        return vehicleCrossingTime;
    }
}
